import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";

export const collections: { events?: mongoDB.Collection } = {};

export async function connectToDatabase() {
    if (!process.env["DB_CONN_STRING"] || !process.env["EVENTS_COLLECTION_NAME"]) throw new Error('Missing required environment variables');
    // Pulls in the .env file so it can be accessed from process.env. No path as .env is in root, the default location
    dotenv.config();

    // Create a new MongoDB client with the connection string from .env
    const client: mongoDB.MongoClient = new mongoDB.MongoClient(process.env["DB_CONN_STRING"]);

    // Connect to the cluster
    await client.connect();

    // Connect to the database with the name specified in .env
    const db: mongoDB.Db = client.db(process.env["DB_NAME"]);

    // Connect to the collection with the specific name from .env, found in the database previously specified
    const eventsCollection: mongoDB.Collection = db.collection(process.env["EVENTS_COLLECTION_NAME"]);

    // Persist the connection to the Games collection
    collections.events = eventsCollection;

    console.log(
        `Successfully connected to database: ${db.databaseName} and collection: ${eventsCollection.collectionName}`,
    );
}