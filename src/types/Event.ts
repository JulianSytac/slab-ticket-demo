export interface Event {
    id?: number;
    name: string;
    category: string;
    date: Date;
    price: string;
}