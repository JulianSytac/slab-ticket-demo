import { Component } from '@angular/core';
import { Event } from '../types/Event';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, retry } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { HttpErrorHandler, HandleError } from '../services/http-error-handler.service';

@Component({
  selector: 'app-root',
  template: `
  <main>
  <h1>sLab Ticket Demo</h1>
    <event-list
      [events]="events"
      [loading]="loading"
      (addEvent)="addEvent($event)"
    ></event-list>
  </main>
  <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private handleError: HandleError;

  events: Event[] = [];
  loading: boolean = true;
  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('EventsService');
  }

  ngOnInit() {
    this.getEvents();
  }

  getEvents(): Subscription {
    return this.http.get<Event[]>('/.netlify/functions/events')
      .pipe(
        retry(3),
        catchError(this.handleError('getEvents', []))
      ).pipe(
        finalize(() => this.loading = false)
      )
      .subscribe(events => (this.events = events.map((event) => {
        return {
          ...event,
          date: new Date(event.date)
        };
      })));
  }
  addEvent(event: Event): void {
    this.addEventRequest(event).subscribe(event => this.events.push(event))
  }
  addEventRequest(event: Event): Observable<Event> {
    return this.http.post<Event>('/.netlify/functions/addEvent', event)
      .pipe(
        catchError(this.handleError('addEvent', event))
      ).pipe(
        finalize(() => this.getEvents())
      );
  }
}
