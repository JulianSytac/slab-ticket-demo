import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Event } from '../types/Event';

@Component({
  selector: 'event-list',
  template: `
    <div class="event-list">
      <h2 class="event-list__list-title">Events</h2>
      <div *ngIf="loading;else event_list">Loading</div>
      <ng-template #event_list>
        <div class="event-list__list" #event_list>
          <event-list-item *ngFor="let event of sortedEvents" [event]="event">
          </event-list-item>
        </div>
        <event-list-add
        (addEvent)="emitAddEvent($event)"
        ></event-list-add>
      </ng-template>
    </div>
  `,
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent {
  @Input() events!: Event[];
  @Input() loading!: boolean;
  @Output() addEvent = new EventEmitter<Event>();

  emitAddEvent(event: Event){
    this.addEvent.emit(event);
  }
  
  get sortedEvents() {
    return this.events?.sort((a, b) => a.date.getTime() - b.date.getTime())
  }
}
