import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Event } from '../types/Event';

@Component({
  selector: 'event-list-add',
  template: `
    <div class="event-list-add">
      <h3>Add new event</h3>
      <div class="event-list-add__input-container">
        <label for="eventName" class="event-list-add__input-label">
        Name
        </label>
        <input name="eventName" id="eventName" type="text" [(ngModel)]="name" class="event-list-add__input"/>
      </div>
      <div class="event-list-add__input-container">
        <label for="eventDate" class="event-list-add__input-label">
        Date
        </label>
        <input name="eventDate" id="eventDate" type="date" [(ngModel)]="date" class="event-list-add__input"/>
      </div>
      <div class="event-list-add__input-container">
        <label for="eventCategory" class="event-list-add__input-label">
        Category
        </label>
        <input name="eventCategory" id="eventCategory" type="text" [(ngModel)]="category" class="event-list-add__input"/>
      </div>
      <div class="event-list-add__input-container">
        <label for="eventPrice" class="event-list-add__input-label">
        Price
        </label>
        <input name="eventPrice" id="eventPrice" type="text" [(ngModel)]="price" class="event-list-add__input"/>
      </div>
      <button (click)="validateAddEvent()" class="event-list-add__button">
        Add Event
      </button>
    </div>
  `,
  styleUrls: ['./event-list-add.component.scss']
})
export class EventListAddComponent {
  name: string = '';
  category: string = '';
  date: Date = new Date();
  price: string = '';
  @Output() addEvent = new EventEmitter<Event>();

  validateAddEvent(){
    if(!this.name || !this.category || !this.date || !this.price) return;
    const newEvent: Event = {
      name: this.name,
      category: this.category,
      date: this.date,
      price: this.price
    };
    this.addEvent.emit(newEvent);
  }
}
