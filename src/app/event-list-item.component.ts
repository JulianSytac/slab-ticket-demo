import { Component, Input } from '@angular/core';
import { Event } from '../types/Event';

@Component({
  selector: 'event-list-item',
  template: `
    <div class="event-list-item">
      <h3 class="event-list-item__name">{{event.name}}</h3>
      <p class="event-list-item__date">{{event.date.toLocaleDateString()}}</p>
      <p class="event-list-item__category">{{event.category}}</p>
      <p class="event-list-item__price">{{event.price}}</p>
    </div>
  `,
  styleUrls: ['./event-list-item.component.scss']
})
export class EventListItemComponent {
  @Input() event!: Event;
}
