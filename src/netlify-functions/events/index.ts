import { Handler } from '@netlify/functions'
import { connectToDatabase, collections } from '../../services/database.service';
import { Event } from '../../types/Event';

export const handler: Handler = async (event, context, callback) => {
  try {
    await connectToDatabase();
    const events = await collections.events?.find({}).toArray() as Event[];
    return {
      statusCode: 200,
      body: JSON.stringify(events)
    };
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify(error)
    };
  }

}
