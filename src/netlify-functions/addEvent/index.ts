import { Handler } from '@netlify/functions'
import { connectToDatabase, collections } from '../../services/database.service';
import querystring from 'querystring';


export const handler: Handler = async (event, context, callback) => {
    console.log(event)
    if (event.httpMethod !== "POST") {
        return { statusCode: 405, body: "Method Not Allowed" };
    }
    try {
        if (!event.body) throw new Error();
        await connectToDatabase();
        const { name, category, date, price } =  JSON.parse(event.body);
        const events = await collections.events?.insertOne({
            name,
            category,
            date,
            price
        });
        return {
            statusCode: 200,
            body: JSON.stringify(events)
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(error)
        };
    }

}
